import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import './assets/sass/style.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import store from './store';
import './i18next';
import Loading from './blocks/loading/Loading';

ReactDOM.render( 
    <Suspense fallback={(<div>Loading</div>)}>
        <Provider store={store}>
            <App />
        </Provider>
    </Suspense>,
    document.getElementById( 'root' ) 
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
