import React from 'react';
import { Link } from 'react-scroll';
import { useDispatch, useSelector } from "react-redux";
import { signout } from '../../actions/userAction';
import { useHistory } from "react-router-dom";
import { useTranslation } from 'react-i18next';

const HomeModalMenu = (props) => {
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const dispatch = useDispatch();
  const signoutHandler = () => {
    dispatch(signout());
    window.location.reload();
  }
  const { t, i18n } = useTranslation();
  return (
    <nav className="menu-primary">
      <ul className="nav flex-column">
        <li className="nav-item">
          <Link onClick={props.action} to="about-us" title="About Us" href="#about-us" smooth={true} duration={700}>{t('About Us.1')}</Link>
        </li>

        <li className="nav-item">
          <Link onClick={props.action} to="services" title="Services" href="#services" smooth={true} duration={700}>{t('Services.1')}</Link>
        </li>

        <li className="nav-item">
          <Link onClick={props.action} to="our-team" title="Prediction" href="#our-team" smooth={true} duration={700}>{t('Prediction.1')}</Link>
        </li>

        <li className="nav-item">
          <Link onClick={props.action} to="testimonials" title="Testimonials" href="offers" smooth={true} duration={700}>{t('Offers.1')}</Link>
        </li>

        <li className="nav-item">
          <Link onClick={props.action} to="news" title="News" href="#news" smooth={true} duration={700}>{t('Products.1')}</Link>
        </li>

        <li className="nav-item">
          <Link onClick={props.action} to="contacts" title="Contacts" href="#contacts" smooth={true} duration={700}>{t('Contacts.1')}</Link>
        </li>
        {userInfo ? (
          <li className="nav-item dropdown">
            <Link to="#">
              {userInfo.name}
              <i className="fa fa-caret-down"></i>
            </Link>
            <ul className="dropdown-content">
              <li>
                <a href={process.env.PUBLIC_URL + "/profile"}>{t("User Profile.1")}</a>
              </li>
              {userInfo && userInfo.isAdmin ? (
                <>
                  <li>
                    <a href={process.env.PUBLIC_URL + "/orderlist"}>Order List</a>
                  </li>
                  <li>
                    <a href={process.env.PUBLIC_URL + "/userlist"}>User List</a>
                  </li>
                  <li>
                    <a href={process.env.PUBLIC_URL + "/productlist"}>Product List</a>
                  </li>
                  <li>
                    <a href={process.env.PUBLIC_URL + "/userlistoffers"}>User Offers List</a>
                  </li>
                </>) : (
                <>
                </>)}
              <li>
                <a href={process.env.PUBLIC_URL + "/orderhistory"}>{t("Order History.1")}</a>
              </li>
              <>
                <li>
                  <Link to="signout" onClick={signoutHandler}>
                    {t("Sign Out.1")}
                  </Link>
                </li>
              </>
            </ul>
          </li>
        ) :
          (<li id="signin" className="nav-item">
            <a to="signin" title="Signin" href="/signin" smooth={true} duration={700}>{t('Signin.1')}</a>
          </li>)}
      </ul>
    </nav>
  );
};

export default HomeModalMenu;
