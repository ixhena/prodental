import React from "react";
import { useTranslation } from 'react-i18next';
import { Link } from 'react-scroll';
import { useDispatch, useSelector } from "react-redux";
import { signout } from '../../actions/userAction';
const ModalMenuPrimary = (props) => {
  const { t, i18n } = useTranslation();
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const dispatch = useDispatch();
  const signoutHandler = () => {
    dispatch(signout());
    window.location.reload();
  }
  return (
    <nav className="menu-primary">
      <ul className="nav flex-column">
        <li className="nav-item">
          <a title="About Us" href={process.env.PUBLIC_URL + "/#about-us"}>
            {t('About Us.1')}
          </a>
        </li>

        <li className="nav-item">
          <a title="Services" href={process.env.PUBLIC_URL + "/#services"}>
            {t('Services.1')}
          </a>
        </li>

        <li className="nav-item">
          <a title="Prediction" href={process.env.PUBLIC_URL + "/#our-team"}>
            {t('Prediction.1')}
          </a>
        </li>

        <li className="nav-item">
          <a
            title="Testimonials"
            href={process.env.PUBLIC_URL + "/offers"}
          >
            {t('Offers.1')}
          </a>
        </li>

        <li
          className={
            "nav-item " +
            (window.location.pathname.includes("/news/Endodonti") ||
              window.location.pathname.includes("/product")
              ? "current-nav-item"
              : "")
          }
        >
          <a title="News" href={process.env.PUBLIC_URL + "/news/Endodonti"}>
            {t('Products.1')}
          </a>
        </li>

        <li className="nav-item">
          <a title="Contacts" href={process.env.PUBLIC_URL + "/#contacts"}>
            {t('Contacts.1')}
          </a>
        </li>

        <li className="nav-item">
          {userInfo ? (
            <li className="nav-item dropdown">
              <Link to="#">
                {userInfo.name}
                <i className="fa fa-caret-down"></i>
              </Link>
              <ul className="dropdown-content">
                <li>
                  <a href={process.env.PUBLIC_URL + "/profile"}>{t("User Profile.1")}</a>
                </li>
                {userInfo && userInfo.isAdmin ? (
                  <>
                    <li>
                      <a href={process.env.PUBLIC_URL + "/orderlist"}>Order List</a>
                    </li>
                    <li>
                      <a href={process.env.PUBLIC_URL + "/userlist"}>User List</a>
                    </li>
                    <li>
                      <a href={process.env.PUBLIC_URL + "/productlist"}>Product List</a>
                    </li>
                    <li>
                      <a href={process.env.PUBLIC_URL + "/userlistoffers"}>User Offers List</a>
                    </li>
                  </>) : (<>
                  </>)}
                <li>
                  <a href={process.env.PUBLIC_URL + "/orderhistory"}>{t("Order History.1")}</a>
                </li>
                <li>
                  <Link to="#signout" onClick={signoutHandler}>
                    {t("Sign Out.1")}
                  </Link>
                </li>
              </ul>
            </li>
          ) :
            (<li id="signin" className="nav-item">
              <a to="signin" title="Signin" href="/signin" smooth={true} duration={700}>{t('Signin.1')}</a>
            </li>)}
        </li>
      </ul>
    </nav>
  );
};

export default ModalMenuPrimary;
