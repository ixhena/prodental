import React from "react";

import CommentsReply from "../../components/button/CommentsReply";
import CommentForm from "../../components/form/CommentForm";

const NewsComments = () => {
  return (
    <div id="comments" className="comments-area spacer m-top-xl">
      <div className="comment-title">
        <h3>1 Comment</h3>
      </div>

      <ul className="comment-list list-unstyled style-default">
        <li className="comment">
          <div className="comment-wrapper">
            <div className="comment-details">
              <div className="comment-content">
                <div className="comment-content-left">
                  <div className="comment-img">
                    <img
                      src="assets/img/placeholder/CONFI-LOGO.jpeg"
                      alt="Rosie Ford"
                    />
                  </div>
                </div>

                <div className="comment-content-right">
                  <h5 className="comment-author after">
                    <a
                      title="Tommy Andersen"
                      href={process.env.PUBLIC_URL + "/product"}
                    >
                      Rosie Ford
                    </a>
                  </h5>

                  <div className="comment-time">
                    <p>29 February, {new Date().getFullYear()} 3:23 pm</p>
                  </div>

                  <div className="comment-description">
                    <p>
                      No one rejects, dislikes, or avoids pleasure itself,
                      because it is pleasure, but because those who do not know
                      how to pursue pleasure rationally.
                    </p>
                  </div>

                  <CommentsReply />
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>

      <div className="block spacer m-top-xl">
        <div id="respond" className="comment-respond">
          <h3 id="reply-title" className="comment-reply-title">
            Leave a reply{" "}
            <a
              rel="nofollow"
              id="cancel-comment-reply-link"
              className="btn btn-link border-0 p-0 min-w-auto link-no-space"
              href={process.env.PUBLIC_URL + "/blog-single-post"}
              style={{ display: "none" }}
            >
              Cancel reply
            </a>
          </h3>

          <CommentForm />
        </div>
      </div>
    </div>
  );
};

export default NewsComments;
