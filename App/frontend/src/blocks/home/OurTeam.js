import React, { useState } from 'react';
import Fade from '@material-ui/core/Fade';
import { useTranslation } from 'react-i18next';
const OurTeam = () => {
    const [open_1, setOpen1] = useState(false);
    const [open_2, setOpen2] = useState(false);
    const [open_3, setOpen3] = useState(false);

    const handleClick = (id) => {
        if (id === 1) {
            setOpen1((prev) => !prev);
        }

        if (id === 2) {
            setOpen2((prev) => !prev);
        }

        if (id === 3) {
            setOpen3((prev) => !prev);
        }

    };
    const { t } = useTranslation();
    return (
        <section id="our-team" className="block spacer p-top-xl">
            <div className="wrapper">

                <div className="row gutter-width-md with-pb-md">
                    <div className="col-sm-4 col-sm-4">
                        <h1 className="team-member-t-head">{t('Mission.1')}</h1>
                        <div className="team-member">
                            <div className="team-member-top">
                                <div className="ourteamimage">
                                    <div className="ourteamimage">
                                        <img className="ourteamimage" src="assets/img/placeholder/mission.jpeg" alt="Tom Henders" />
                                    </div>
                                </div>
                            </div>

                            <div className="team-member-content">
                                <div className="team-member-position">
                                    {/* <p>Head Doctor</p> */}
                                </div>
                                <div className="team-member-description">
                                    <p> {t('Import-eksport.1')} </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-4 col-sm-4">
                        <h1 className="team-member-t-head">{t('Vision.1')}</h1>
                        <div className="team-member">
                            <div className="team-member-top">
                                <div className="ourteamimage">
                                    <div className="ourteamimage">
                                        <img className="ourteamimage" src="assets/img/placeholder/our-mission.jpeg" />
                                    </div>
                                </div>
                            </div>

                            <div className="team-member-content">
                                <div className="team-member-position">
                                    {/* <p>Head Doctor</p> */}
                                </div>

                                <div className="team-member-description">
                                    <p>{t('To be a leader in the market of dental materials.1')}</p>
                                    <p>{t('To be producers of various innovative products.1')}</p>
                                    <p>{t('To be an exporter to European countries.1')}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default OurTeam;
