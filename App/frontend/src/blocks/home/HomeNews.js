import React, { Fragment, useEffect } from "react";
import HomeNewsItems from "../../data/news/newsHomeItems.json";
import { useDispatch, useSelector } from "react-redux";
import { listProducts } from "../../actions/productActions";
import { Button, Card } from 'react-bootstrap';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';
import { useTranslation } from "react-i18next";
import { isMobile } from 'react-device-detect';

const HomeNews = () => {
  const dispatch = useDispatch();
  const productList = useSelector((state) => state.productList);
  const { loading, error, products } = productList;
  const slideImages = [
    '/uploads/1630797291429.jpg',
    '/uploads/1632819292211.jpg',
    '/uploads/1632823017246.jpg'
  ];
  const { t } = useTranslation();
  useEffect(() => {
    dispatch(listProducts());
  }, []);
  return (
    <section id="news" className="block spacer p-top-xl">
      <div className="wrapper">
        <div className="title">
          <h2>{t('Our Products.1')}</h2>
        </div>
        <div class={isMobile ? "" : "slide"}>
          <Slide easing="ease">
            <div className="each-slide">
              <img src="/uploads/1630797291429.jpg" style={{ height: "100%", width: "100%", background: 'center', backgroundColor: '#385bb2', backgroundRepeat: 'no-repeat' }}>
              </img>
            </div>
            <div className="each-slide">
              <img src="/uploads/1632819292211.jpg" style={{ height: "100%", width: "100%", background: 'center', backgroundColor: '#385bb2', backgroundRepeat: 'no-repeat' }}>
              </img>
            </div>
            <div className="each-slide">
              <img src="/uploads/1632823017246.jpg" style={{ height: "100%", width: "100%", background: 'center', backgroundColor: '#385bb2', backgroundRepeat: 'no-repeat' }}>
              </img>
            </div>
          </Slide>
        </div>
        <div className="spacer m-top-lg text-right">
          <a
            title="View all news"
            className="btn btn-primary"
            href={process.env.PUBLIC_URL + "/news/Endodonti"}
          >
            {t('View All Products.1')}
          </a>
        </div>
      </div>
    </section>
  );
};

export default HomeNews;
