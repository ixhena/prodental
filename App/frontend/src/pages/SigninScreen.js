import React, { Fragment, useEffect, useState } from "react";
import Header from "../blocks/header/Header";
import Footer from "../blocks/footer/Footer";
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import '../index.css';
import Loading from "../blocks/loading/Loading";
import MessageBox from '../components/MessageBox';
import { signin } from '../actions/userAction';
import { useLocation, useNavigate } from 'react-router-dom';

const SigninScreen = (props) => {
  document.body.classList.add("signin");
  const navigate = useNavigate();
  let location = useLocation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const redirect = location.search
    ? location.search.split('=')[1]
    : '/';
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, error } = userSignin;
  const dispatch = useDispatch();
  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(signin(email, password));
  }
  useEffect(() => {
    if (userInfo) {
      if (location.search == "") {
        navigate("/")
      } else {
        navigate("/shipping");
      }
    }
  }, [props.history, redirect, userInfo]);
  return (
    <div>
      <Header />
      <form className="form" onSubmit={submitHandler}>
        <div>
          <h1>Sign In</h1>
        </div>
        {<Loading></Loading>}
        {<MessageBox variant="danger">{error}</MessageBox>}
        <div>
          <label htmlFor="email">Email address</label>
          <input
            type="email"
            id="email"
            placeholder="Enter email"
            required
            onChange={(e) => setEmail(e.target.value)}
          ></input>
        </div>
        <div>
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            placeholder="Enter password"
            required
            onChange={(e) => setPassword(e.target.value)}
          ></input>
        </div>
        <div>
          <label />
          <button className="primary" type="submit">
            Sign In
          </button>
        </div>
        <div>
          <label />
          <div>
            Forgot Password?{' '}
            <Link to="forget-password">Update your password</Link>
          </div>
          <div>
            New customer?{' '}
            <Link to={`/register?redirect=${redirect}`}>
              Create your account
            </Link>
          </div>
        </div>
      </form>
      <Footer />
    </div>
  );
};
export default SigninScreen;
