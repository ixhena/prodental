import React, { Fragment, useEffect, useState } from "react";
import MetaTags from "react-meta-tags";
import Loading from "../blocks/loading/Loading";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Header from "../blocks/header/Header";
import Footer from "../blocks/footer/Footer";
import { useDispatch, useSelector } from 'react-redux';
import Axios from 'axios';
import { detailsProduct, updateProduct } from '../actions/productActions';
import LoadingBox from '../components/loadmore/LoadMore';
import MessageBox from '../components/MessageBox';
import { PRODUCT_UPDATE_RESET } from '../constants/productConstants';
import { useLocation, useNavigate, useParams } from 'react-router-dom';

export default function ProductEditScreen() {
  const { id } = useParams();
  const navigate = useNavigate();
  const productId = id;
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [image, setImage] = useState('');
  const [category, setCategory] = useState('');
  const [countInStock, setCountInStock] = useState('');
  const [brand, setBrand] = useState('');
  const [isOffer, setisOffer] = useState('');
  const [isUserOffer, setisUserOffer] = useState('');
  const [description, setDescription] = useState('');
  const [date, setDate] = useState(new Date());
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [discount, setDiscount] = useState('');
  let [discountPrice, setDiscountPrice] = useState();
  const productDetails = useSelector((state) => state.productDetails);
  const { loading, error, product } = productDetails;
  const productUpdate = useSelector((state) => state.productUpdate);
  const {
    loading: loadingUpdate,
    error: errorUpdate,
    success: successUpdate,
  } = productUpdate;

  const dispatch = useDispatch();
  useEffect(() => {
    if (successUpdate) {
      navigate('/productlist');
    }

    if (!product || product._id !== productId || successUpdate) {
      dispatch({ type: PRODUCT_UPDATE_RESET });
      dispatch(detailsProduct(productId));
    } else {
      setName(product.name);
      setPrice(product.price);
      setImage(product.image);
      setCategory(product.category);
      setCountInStock(product.countInStock);
      setBrand(product.brand);
      setDescription(product.description);
      console.log(product.startDate)
      product !== undefined && product.startDate !== undefined || product.startDate !== "" ?
        setStartDate(new Date(product.startDate)) : setStartDate(new Date());
      product !== undefined && product.endDate !== undefined ?
        setEndDate(new Date(product.endDate)) : setStartDate(new Date());
      setDiscount(product.discount);
      setDiscountPrice(product.discountPrice);
      setisOffer(product.isOffer);
      setisUserOffer(product.isUserOffer);
    }
  }, [product, dispatch, productId, successUpdate, navigate]);
  const submitHandler = (e) => {
    e.preventDefault();
    // TODO: dispatch update product
    dispatch(
      updateProduct({
        _id: productId,
        name,
        price,
        discount,
        discountPrice,
        startDate,
        endDate,
        image,
        category,
        brand,
        countInStock,
        description,
        isOffer,
        isUserOffer
      })
    );
  };
  const [loadingUpload, setLoadingUpload] = useState(false);
  const [errorUpload, setErrorUpload] = useState('');
  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const uploadFileHandler = async (e) => {
    const file = e.target.files[0];
    const bodyFormData = new FormData();
    bodyFormData.append('image', file);
    setLoadingUpload(true);
    try {
      const { data } = await Axios.post('/api/uploads', bodyFormData, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${userInfo.token}`,
        },
      });
      setImage(data);
      setLoadingUpload(false);
    } catch (error) {
      setErrorUpload(error.message);
      setLoadingUpload(false);
    }
  };
  if (product !== undefined) {
    if (product.price > 0 && product.startDate !== "" && product.endDate !== "" && product.discount > 0) {
      var tzoffsetdate = (new Date(date)).getTimezoneOffset() * 60000;
      var date1 = (new Date(date - tzoffsetdate)).toISOString().slice(0, -1);
      date1 = date1.substr(0, date1.indexOf('T'));
      var date2 = product.startDate.substr(0, product.startDate.indexOf('T'));
      var date3 = product.endDate.substr(0, product.endDate.indexOf('T'));
      if (date2 >= date1 && date1 <= date3 && product.discount > 0 && product !== undefined) {
        discountPrice = Math.round(product.price - (product.price / 100) * discount);
      } else {
        discountPrice = "";
      }
    }
  }

  return (
    <>
      {loadingUpdate && <LoadingBox></LoadingBox>}
      {errorUpdate && <MessageBox variant="danger">{errorUpdate}</MessageBox>}
      {loading ? (
        <LoadingBox></LoadingBox>
      ) : error ? (
        <MessageBox variant="danger">{error}</MessageBox>
      ) : (
        <Fragment>
          <Loading />
          <MetaTags>
            <meta charSet="UTF-8" />
            <title>
              News single post | Hosco - Dentist & Medical React JS Template
            </title>

            <meta httpEquiv="x-ua-compatible" content="ie=edge" />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1"
            />
            <meta name="description" content="" />
            <meta name="keywords" content="" />
            <meta name="robots" content="index, follow, noodp" />
            <meta name="googlebot" content="index, follow" />
            <meta name="google" content="notranslate" />
            <meta name="format-detection" content="telephone=no" />
          </MetaTags>


          <Header />
          <div>
            <form className="form" >
              <div>
                <h1>Edit Product {productId}</h1>
              </div>
              <div>
                <label htmlFor="name">Name</label>
                <input
                  id="name"
                  type="text"
                  placeholder="Enter name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                ></input>
              </div>
              <div>
                <label htmlFor="price">Price</label>
                <input
                  id="price"
                  type="text"
                  placeholder="Enter price"
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                ></input>
              </div>
              <div>
                <label htmlFor="image">Image</label>
                <input
                  id="image"
                  type="text"
                  placeholder="Enter image"
                  value={image}
                  onChange={(e) => setImage(e.target.value)}
                ></input>
              </div>
              <div>
                <label htmlFor="imageFile">Image File</label>
                <input
                  type="file"
                  id="imageFile"
                  label="Choose Image"
                  onChange={uploadFileHandler}
                ></input>
                {loadingUpload && <LoadingBox></LoadingBox>}
                {errorUpload && (
                  <MessageBox variant="danger">{errorUpload}</MessageBox>
                )}
              </div>
              <div>
                <label htmlFor="category">Category/User Name that profit the offer</label>
                <input
                  id="category"
                  type="text"
                  placeholder="Enter category"
                  value={category}
                  onChange={(e) => setCategory(e.target.value)}
                ></input>
              </div>
              <div>
                <label htmlFor="brand">Brand</label>
                <input
                  id="brand"
                  type="text"
                  placeholder="Enter brand"
                  value={brand}
                  onChange={(e) => setBrand(e.target.value)}
                ></input>
              </div>
              <div>
                <label htmlFor="countInStock">Count In Stock</label>
                <input
                  id="countInStock"
                  type="text"
                  placeholder="Enter countInStock"
                  value={countInStock}
                  onChange={(e) => setCountInStock(e.target.value)}
                ></input>
              </div>
              <div>
                <label >Discount Start Date</label>
                <DatePicker
                  id="startDate"
                  selected={startDate}
                  onChange={(date) => setStartDate(date)}
                  selectsStart
                  startDate={startDate}
                  endDate={endDate}
                />
              </div>
              <div>
                <label >Discount End Date</label>
                <DatePicker
                  id="endDate"
                  selected={endDate}
                  onChange={(date) => setEndDate(date)}
                  selectsEnd
                  startDate={startDate}
                  endDate={endDate}
                  minDate={startDate}
                />
              </div>
              <div>
                <label >Discount -%</label>
                <input
                  id="discount"
                  type="text"
                  placeholder="Enter discount"
                  value={discount}
                  onChange={(e) => setDiscount(e.target.value)}
                ></input>
              </div>
              <div>
                <label>Discount Price</label>
                <input
                  id="discountPrice"
                  placeholder="Discount Price"
                  value={discountPrice}
                ></input>
              </div>
              <div>
                <label htmlFor="isOffer">Is Offer</label>
                <input
                  name="isOffer"
                  id="isOffer"
                  type="checkbox"
                  checked={isOffer}
                  onChange={(e) => setisOffer(e.target.checked)}
                ></input>
                <label htmlFor="isUserOffer">Is User Offer</label>
                <input
                  name="isUserOffer"
                  id="isUserOffer"
                  type="checkbox"
                  checked={isUserOffer}
                  onChange={(e) => setisUserOffer(e.target.checked)}
                ></input>
              </div>
              <div>
                <label htmlFor="description">Description</label>
                <textarea
                  id="description"
                  rows="3"
                  type="text"
                  placeholder="Enter description"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                ></textarea>
              </div>
              <div>
                <label></label>
                <button className="primary" type="submit" onClick={submitHandler}>
                  Update
                </button>
              </div>

            </form>
            <div style={{ marginTop: "150px" }}>
              <Footer></Footer>
            </div>
          </div>
        </Fragment>
      )
      }
    </>
  );
}
