import React, {
  Fragment,
  useEffect,
  useState
} from "react";
import MetaTags from "react-meta-tags";

import Loading from "../blocks/loading/Loading";
import Header from "../blocks/header/Header";
import Footer from "../blocks/footer/Footer";
import {
  useDispatch,
  useSelector
} from "react-redux";
import {
  Link
} from "react-router-dom";
import {
  addToCart,
  removeFromCart
} from "../actions/cartActions";
import MessageBox from "../components/MessageBox";
import {
  data
} from "../data";
import {
  FontAwesomeIcon
} from '@fortawesome/react-fontawesome'
import {
  faLock
} from '@fortawesome/free-solid-svg-icons'
import "../index.css";
import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate, useParams } from 'react-router-dom';

const Cart = (props) => {
  let location = useLocation();
  const { _id } = useParams();
  const navigate = useNavigate();
  const qty = location.search ?
    Number(location.search.split("=")[1]) :
    1;
  const userSignin = useSelector((state) => state.userSignin);
  const {
    userInfo
  } = userSignin;
  const cart = useSelector((state) => state.cart);
  const {
    cartItems
  } = cart;
  var [date, setDate] = useState(new Date());
  var name;
  var price;
  var description;
  var countInStock;
  var id;
  var discountPrice;
  id = _id;
  cartItems.map((e) => {
    name = e.name;
    price = e.price;
    description = e.description;
    countInStock = e.countInStock;
    discountPrice = e.discountPrice;
  });
  let stock = [];
  for (var i = 0; i <= countInStock; i++) {
    stock.push(i);
  }
  const dispatch = useDispatch();
  useEffect(() => {
    if (id) {
      dispatch(addToCart(id, qty));
    }
  }, [dispatch, id, qty]);
  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id));
  };
  const checkoutHandler = () => {
    navigate("/signin?redirect=shipping");
  };
  const signinFirst = (props) => {
    navigate(`/signin?redirect=/product/${id}`)
  }
  var tzoffsetdate = (new Date(date)).getTimezoneOffset() * 60000;
  var dateInitial = (new Date(date - tzoffsetdate)).toISOString().slice(0, -1);
  dateInitial = dateInitial.substr(0, dateInitial.indexOf('T'));
  const { t, i18n } = useTranslation();

  return (
    <Fragment>
      <MetaTags>
        <meta charSet="UTF-8" />

        <meta httpEquiv="x-ua-compatible"
          content="ie=edge" />
        <meta name="viewport"
          content="width=device-width, initial-scale=1" /
        >
        <meta name="description"
          content="" />
        <meta name="keywords"
          content="" />
        <meta name="robots"
          content="index, follow, noodp" />
        <meta name="googlebot"
          content="index, follow" />
        <meta name="google"
          content="notranslate" />
        <meta name="format-detection"
          content="telephone=no" />
      </MetaTags>

      <Loading />

      <Header />
      <div className="row top" >
        <div className="col-2" >
          <h1 > {t('ShoppingCart.1')} </h1> {
            cartItems.length === 0 ? (
              <MessageBox >
                {t("CartIsEmpty.1")} < Link to="/" > {t("GoShopping.1")} </Link>
              </MessageBox>
            ) : (<ul > {
              cartItems.map((item) => (<li >
                <div className="row" >
                  <div >
                    <img src={
                      item.image
                    }
                      alt={
                        item.name
                      }
                      className="small" >
                    </img>
                  </div>
                  <div className="min-30" >
                    <a
                      href={
                        process.env.PUBLIC_URL +
                        "/product" +
                        "/" +
                        item.id
                      }
                    >
                      {item.name}
                    </a>
                  </div>
                  <div >
                    <select value={
                      item.qty
                    }
                      onChange={
                        (e) =>
                          dispatch(
                            addToCart(item.product, Number(e.target.value))
                          )
                      } >
                      {
                        stock.map((x) => (<option key={
                          x + 1
                        }
                          value={
                            x + 1
                          } > {
                            x + 1
                          } </option>
                        ))} </select> </div>
                  {
                    userInfo !== null ?
                      <div>{(item.endDate >= dateInitial ? item.discountPrice || item.price : item.price)}ALL</div>
                      : <></>} <div >
                    <button type="button"
                      onClick={
                        () => removeFromCartHandler(item.product)
                      } >
                      {t("Delete.1")} </button> </div> </div> </li>
              ))} </ul>
            )} </div> <div className="col-1" >
          <div className="card card-body" >
            <ul >
              <li > {
                userInfo !== null ?
                  <h2 >
                    {t("Subtotal.1")}({
                      cartItems.reduce((a, c) => a + c.qty, 0)
                    }
                    {t("items.1")}) :
                    {cartItems.reduce((a, c) =>
                      a + (c.endDate > dateInitial ? c.discountPrice || c.price : c.price) * c.qty, 0)}ALL
                  </h2> : <h2>{t("Subtotal.1")}<a onClick={
                    () => signinFirst(props)
                  } >
                    <FontAwesomeIcon icon={
                      faLock
                    } > </FontAwesomeIcon> </a>   </h2>} </li> <li >
                <button
                  type="button"
                  onClick={
                    checkoutHandler
                  }
                  className="primary block"
                  disabled={
                    cartItems.length === 0
                  } >
                  {t("ProceedToCheckout.1")}</button> </li> </ul> </div> </div> </div> <div style={
                    {
                      marginTop: "150px"
                    }
                  } >
        <Footer > </Footer> </div> </Fragment>
  );
};
export default Cart;