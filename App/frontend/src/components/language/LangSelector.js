import React from 'react';
import { Link } from 'react-scroll';
import { useDispatch, useSelector } from "react-redux";
import { signout } from '../../actions/userAction';
import OrderHistoryScreen from '../../pages/OrderHistoryScreen';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { useTranslation } from 'react-i18next';
import { isMobile } from 'react-device-detect';

const LangSelector = () => {
    const userSignin = useSelector((state) => state.userSignin);
    const { userInfo } = userSignin;
    const { t, i18n } = useTranslation();
    const dispatch = useDispatch();
    const signoutHandler = () => {
        dispatch(signout());
        window.location.reload();
    }
    const handleClick = (lang) => {
        i18n.changeLanguage(lang);
    }
    return (
        <>
            {isMobile ?
                <nav className="languages">
                    <ul className="nav ">
                        <li className="lang-item current-lang">
                            <a title="En" onClick={() => handleClick('en')}>En</a>
                        </li>

                        <li className="lang-item">
                            <a title="Al" onClick={() => handleClick('al')}>Al</a>
                        </li>
                    </ul>
                </nav> : <nav className="languages">
                    <ul className="nav ">
                        <li class="user">
                            {userInfo ?
                                (
                                    <div className="dropdown">
                                        <Link to="#" >
                                            {userInfo.name}
                                            <i className="fa fa-caret-down"></i>
                                        </Link>
                                        <ul className="dropdown-content">
                                            <li>
                                                <a href={process.env.PUBLIC_URL + "/profile"}>{t("User Profile.1")}</a>
                                            </li>
                                            {userInfo && userInfo.isAdmin ? (
                                                <>
                                                    <li>
                                                        <a href={process.env.PUBLIC_URL + "/orderlist"}>Order List</a>
                                                    </li>
                                                    <li>
                                                        <a href={process.env.PUBLIC_URL + "/userlist"}>User List</a>
                                                    </li>
                                                    <li>
                                                        <a href={process.env.PUBLIC_URL + "/productlist"}>Product List</a>
                                                    </li>
                                                    <li>
                                                        <a href={process.env.PUBLIC_URL + "/userlistoffers"}>User Offers List</a>
                                                    </li>
                                                </>) : (<></>)}
                                            <li>
                                                <a href={process.env.PUBLIC_URL + "/orderhistory"}>{t("Order History.1")}</a>
                                            </li>
                                            <li>
                                                <Link to="#signout" onClick={signoutHandler}>
                                                    {t("Sign Out.1")}
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                ) :
                                (<div id="signin" className="nav-item">
                                    <a to="signin" title="Signin" href="/signin" >{t("Signin.1")}</a>
                                </div>)
                            }</li>
                        <li className="lang-item current-lang">
                            <a title="En" onClick={() => handleClick('en')}>En</a>
                        </li>

                        <li className="lang-item">
                            <a title="Al" onClick={() => handleClick('al')}>Al</a>
                        </li>
                    </ul>
                </nav>
            }
        </>


    );
};

export default LangSelector;
