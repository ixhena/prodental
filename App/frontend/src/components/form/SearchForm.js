import React from 'react';
import ButtonSearch from '../../components/button/ButtonSearch';
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import MessageBox from '../../components/MessageBox';
import { useNavigate, useLocation, useParams } from 'react-router-dom';

const SearchForm = () => {
    const productList = useSelector((state) => state.productList);
    var { loading, error, products } = productList;
    const [searchValue, setSearchValue] = useState("");
    const [errorMessage, setErrorMessage] = useState('');
    const navigate = useNavigate();
    var id;
    var nameUrl;
    var productCateg = ["Endodonti", "Dentistry Restorative", "Cemente dhe shtresa izoluese", "Materiale Mase", "Profilaksi", "Te tjera"]
    var productListing = localStorage.getItem('products');
    products = JSON.parse(productListing);
    Object.keys(products).map((key) => {
        var onewUperCaseLetter = products[key].name.charAt(0).toUpperCase() + products[key].name.slice(1);
        if (searchValue == (products[key].name).toLowerCase() || searchValue == (products[key].name).toUpperCase() || searchValue == onewUperCaseLetter) {
            id = products[key]._id
        }
    })
    Object.keys(productCateg).map((key) => {
        var onewUperCaseLetter = productCateg[key].charAt(0).toUpperCase() + productCateg[key].slice(1);
        if (searchValue == (productCateg[key]).toLowerCase() || searchValue == (productCateg[key]).toUpperCase() || searchValue == onewUperCaseLetter) {
            nameUrl = process.env.PUBLIC_URL + "/news/" + productCateg[key];
        }
    })

    const searchButton = (event) => {
        event.preventDefault();
        if (id !== undefined) {
            navigate(process.env.PUBLIC_URL + "/product" + "/" + id);
            window.location.reload();
        } else if (nameUrl != undefined) {
            navigate(nameUrl);
            window.location.reload();
        } else {
            setErrorMessage("No result !")
        }
    }
    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            if (id !== undefined) {
                navigate(process.env.PUBLIC_URL + "/product" + "/" + id);
                window.location.reload();
            } else if (nameUrl != undefined) {
                navigate(nameUrl);
                window.location.reload();
            } else {
                setErrorMessage("No result !")
            }
        }
    }
    return (
        <form className="search-form" role="search">
            <MessageBox variant="danger">{errorMessage}</MessageBox>
            <div className="search input-group">
                <input className="form-control form-control-lg" type="text" placeholder="Search" onKeyDown={handleKeyDown} onChange={event => { setSearchValue(event.target.value) }} />
                <div className="input-group-append">
                    <ButtonSearch searchButton={searchButton}></ButtonSearch>
                </div>
            </div>
        </form>
    );
};

export default SearchForm;
