import express from "express";
import mongoose from "mongoose";
import userRouter from "../backend/routers/userRouter.js";
import productRouter from "./routers/productRouter.js";
import orderRouter from "../backend/routers/orderRouter.js";
import offersRouter from "../backend/routers/offersRouter.js"
import dotenv from 'dotenv';
import uploadRouter from './routers/uploadRouter.js';
import path from 'path';
import mysql from 'mysql';

dotenv.config();

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const db = mysql.createConnection({
  user: 'root',
  host: 'localhost',
  port: 8889,
  password: 'root',
  database: 'prodental'
})
db.connect((err) => {
  if (err) {
    console.log(`${err}`);
    return;
  }
  console.log('Connection established');
});

app.use('/api/uploads', uploadRouter);
app.use("/api/users", userRouter);
app.use("/api/products", productRouter);
app.use("/api/orders", orderRouter);
app.use("/api/offers", offersRouter);
app.get("/");
app.get('/api/config/google', (req, res) => {
  res.send(process.env.REACT_APP_API_KEY || '');
});
app.get('/api/config/paypal', (req, res) => {
  res.send(process.env.PAYPAL_CLIENT_ID || 'sb');
});
const __dirname = path.resolve();
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));
app.use(express.static(path.join(__dirname, '/frontend/build')));
app.get('*', (req, res) =>
  res.sendFile(path.join(__dirname, '/frontend/build/index.html'))
);
app.use((err, req, res, next) => {
  res.status(500).send({ message: err.message });
});
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server at http://localhost:${port}`);
});
