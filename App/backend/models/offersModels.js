import mongoose from 'mongoose';

const offersSchema = new mongoose.Schema(
    {
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
        description: { type: String, required: false },
        price: { type: Number, required: true },
        currency: { type: String, required: true },
        approved: { type: Boolean, default: false, required: false },
        notapproved: { type: Boolean, default: false, required: false },
    },
    {
        timestamps: true,
    }
);
const Offers = mongoose.model('offers', offersSchema);
export default Offers;