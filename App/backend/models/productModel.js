import mongoose from "mongoose";

const productSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, unique: true },
    seller: { type: mongoose.Schema.Types.ObjectID, ref: 'User' },
    category: { type: String, required: true },
    title: { type: String, required: false },
    image: { type: String, required: false },
    description: { type: String, required: true },
    price: { type: Number, required: true },
    discount: { type: Number, required: false },
    discountPrice: { type: Number, required: false },
    startDate: { type: String, required: false },
    endDate: { type: String, required: false },
    countInStock: { type: Number, required: true },
    brand: { type: String, required: true },
    rating: { type: Number, required: true },
    numReviews: { type: Number, required: true },
    isOffer: { type: Boolean, default: false, required: false },
    isUserOffer: { type: Boolean, default: false, required: false },
  },
  {
    timestamps: true,
  }
);

const Product = mongoose.model("Product", productSchema);

export default Product;
