import expressAsyncHandler from "express-async-handler";
import { data } from "../data.js";
import Product from "../models/productModel.js";
import User from '../models/userModel.js';
import express from "express";
import {
  isAuth,
} from '../utils.js';
import nodemailer from "nodemailer";

const productRouter = express.Router();

productRouter.get(
  "/",
  expressAsyncHandler(async (req, res) => {
    const products = await Product.find({});
    res.send(products);
  })
);
productRouter.get(
  "/seed",
  expressAsyncHandler(async (req, res) => {
    await Product.remove({});
    const createdProducts = await Product.insertMany(data.products);
    res.send({ createdProducts });
  })
);
productRouter.get(
  "/:_id",
  expressAsyncHandler(async (req, res) => {
    const product = await Product.findById(req.params._id);
    if (product) {
      res.send(product);
    } else {
      res.status(400).send({ message: "Product not found" });
    }
  })
);
productRouter.post(
  '/',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const product = new Product({
      name: 'sample name ' + Date.now(),
      image: '/images/p1.jpg',
      price: 0,
      discount: 0,
      discountPrice: 0,
      startDate: '',
      endDate: '',
      category: 'sample category',
      brand: 'sample brand',
      countInStock: 0,
      rating: 0,
      numReviews: 0,
      description: 'sample description',
    });
    const createdProduct = await product.save();
    res.send({ message: 'Product Created', product: createdProduct });
  })
);
productRouter.put(
  '/:id',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const productId = req.params.id;
    const product = await Product.findById(productId);
    const users = await User.find({});
    if (product) {
      product.name = req.body.name;
      product.price = req.body.price;
      product.discount = req.body.discount;
      product.discountPrice = req.body.discountPrice;
      product.startDate = req.body.startDate;
      product.endDate = req.body.endDate;
      product.image = req.body.image;
      product.category = req.body.category;
      product.brand = req.body.brand;
      product.countInStock = req.body.countInStock;
      product.description = req.body.description;
      product.isOffer = req.body.isOffer;
      product.isUserOffer = req.body.isUserOffer;
      const updatedProduct = await product.save();
      if (product.isOffer == true) {
        users.map(user => {
          var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: 'endridogjani4@gmail.com',
              pass: 'prodental'
            }
          });
          var mailOption = {
            from: "endridogjani4@gmail.com",
            to: user.email,
            subject: 'ProDental Offer Suggestion',
            html: "<h1>Welcome To ProDental! </h1><p>\
                    <h3>Hello </h3>\
                    New Offer is available <b><br>\
                    Name : <b>"+ product.name + "<br>\
                    Price: <b>" + product.price + "<br>\
                    Desciption:" + product.description + "<b><br>\
                    Thank you ,<br/>\
                    ProDental<br/>\
                    </p>"
          }

          transporter.sendMail(mailOption, function (error, info) {
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
            }
          })
        })

      }
      res.send({ message: 'Product Updated', product: updatedProduct });
    } else {
      res.status(404).send({ message: 'Product Not Found' });
    }
  })
);
productRouter.delete(
  '/:id',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id);
    if (product) {
      const deleteProduct = await product.remove();
      res.send({ message: 'Product Deleted', product: deleteProduct });
    } else {
      res.status(404).send({ message: 'Product Not Found' });
    }
  })
);

export default productRouter;
