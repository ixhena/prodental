import express from 'express';
import expressAsyncHandler from 'express-async-handler';
import Offers from '../models/offersModels.js';
import User from '../models/userModel.js';
import {
    isAdmin,
    isAuth,
    isSellerOrAdmin,
} from '../utils.js';
import nodemailer from "nodemailer";

const offersRouter = express.Router();
offersRouter.get(
    "/",
    expressAsyncHandler(async (req, res) => {
        const offers = await Offers.find({});
        res.send(offers);
    })
);
offersRouter.post(
    '/',
    isAuth,
    expressAsyncHandler(async (req, res) => {
        const offers = new Offers({
            user: req.user._id,
            price: req.body.price,
            description: req.body.description,
            currency: req.body.currency,
            approved: req.body.approved,
            notapproved: req.body.notapproved
        });
        const createOffers = await offers.save();
        res
            .status(201)
            .send({ message: 'New Offer Created', offers: createOffers, })

    })
);
offersRouter.get('/:id', isAuth, expressAsyncHandler(async (req, res) => {
    const offers = await Offers.findById(req.params.id);
    if (offers) {
        res.send(offers);
    } else {
        res.status(404).send({ message: 'Offers Not Found' })
    }
}))
offersRouter.delete(
    '/:id',
    isAuth,
    expressAsyncHandler(async (req, res) => {
        const offer = await Offers.findById(req.params.id);
        if (offer) {
            const deleteOffers = await offer.remove();
            res.send({ message: 'Offer Deleted', offers: deleteOffers });
            if (offer.approved == false) {
                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'endridogjani4@gmail.com',
                        pass: 'prodental'
                    }
                });
                var mailOption = {
                    from: "endridogjani4@gmail.com",
                    to: req.user.email,
                    subject: 'ProDental Offer Suggestion',
                    html: "<h3>Hello" + " " + req.user.name + "</h3>\
                    We are sorry to inform you that your Offer suggestion with this details:<br><br>\
                    <b>Products :</b>"+ " " + offer.description + "<br /><br>\
                        <b> Price:</b>"+ " " + offer.price + offer.currency + "<br /><br>\
                        <b> Has NOT been approved!<br/><br/>\
                    Best wishes ,<br/>\
                    ProDental<br/>\
                    </p>"
                }

                transporter.sendMail(mailOption, function (error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                    }
                })
            }

        } else {
            res.status(404).send({ message: 'Offer Not Found' });
        }
    })
);
offersRouter.put(
    '/:id',
    isAuth,
    isAdmin,
    expressAsyncHandler(async (req, res) => {
        const offer = await Offers.findById(req.params.id);
        if (offer) {
            offer.description = req.body.description || offer.description;
            offer.price = req.body.price || offer.price;
            offer.currency = req.body.currency || offer.currency;
            offer.approved = Boolean(req.body.approved);
            const updatedOffer = await offer.save();
            res.send({ message: 'Offer Updated', offer: updatedOffer });
        } else {
            res.status(404).send({ message: 'Offer Not Found' });
        }
    })
);
export default offersRouter;