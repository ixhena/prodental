import express from "express";
//import { data } from "../data.js";
//import { User } from "../models/userModel.js";
//import expressAsyncHandler from "express-async-handler";
import bcrypt from "bcryptjs";
import { generateToken, isAdmin, isAuth, status } from '../utils.js';
//import nodemailer from "nodemailer";
import mysql from 'mysql';

const userRouter = express.Router();

const db = mysql.createConnection({
  user: 'root',
  host: 'localhost',
  port: 8889,
  password: 'root',
  database: 'prodental'
})
db.connect((err) => {
  if (err) {
    console.log(`${err}`);
    return;
  }
  console.log('Connection established');
});

// userRouter.get(
//   '/top-sellers',
//   expressAsyncHandler(async (req, res) => {
//     const topSellers = await User.find({ isSeller: true })
//       .sort({ 'seller.rating': -1 })
//       .limit(3);
//     res.send(topSellers);
//   })
// );

// userRouter.get(
//   '/seed',
//   expressAsyncHandler(async (req, res) => {
//     // await User.remove({});
//     const createdUsers = await User.insertMany(data.users);
//     res.send({ createdUsers });
//   })
// );

userRouter.post(
  '/signin',
  (req, res) => {
    let sql1 = "SELECT * FROM `users` WHERE `COL 4`= '" + req.body.email + "'";
    db.query(sql1, (err, results) => {
      if (err) throw err;
      console.log(results.length)
      console.log("MUT")
      if (results.length >= 1) {
        var resultArray = Object.values(JSON.parse(JSON.stringify(results)));
        if (bcrypt.compareSync(req.body.password, resultArray[0]['COL 9'])) {
          if (resultArray[0]['COL 10'] == "true") {
            res.send({
              _id: resultArray[0]['COL 2'],
              name: resultArray[0]['COL 7'],
              nipt: resultArray[0]['COL 8'],
              email: resultArray[0]['COL 4'],
              isAdmin: resultArray[0]['COL 5'],
              status: resultArray[0]['COL 10'],
              isSeller: resultArray[0]['COL 6'],
              token: generateToken(resultArray[0]),
            });
          } else {
            res.status(401).send({ message: 'Your profile is not active yet ' });
          }
        } else {
          res.status(401).send({ message: 'Invalid email or password' });
        }
      } else {
        res.status(401).send({ message: 'You are not registered! ' });
      }
    });
  }
);

// userRouter.post(
//   '/register',
//   expressAsyncHandler(async (req, res) => {
//     const user = new User({
//       name: req.body.name,
//       nipt: req.body.nipt,
//       email: req.body.email,
//       password: bcrypt.hashSync(req.body.password, 8),
//     });
//     const createdUser = await user.save();
//     res.send({
//       _id: createdUser._id,
//       name: createdUser.name,
//       nipt: createdUser.nipt,
//       email: createdUser.email,
//       isAdmin: createdUser.isAdmin,
//       status: user.status,
//       isSeller: user.isSeller,
//       token: generateToken(createdUser),
//     });
//   })
// );

// userRouter.get(
//   '/:id',
//   expressAsyncHandler(async (req, res) => {
//     const user = await User.findById(req.params.id);
//     if (user) {
//       res.send(user);
//     } else {
//       res.status(404).send({ message: 'User Not Found' });
//     }
//   })
// );
// userRouter.put(
//   '/profile',
//   isAuth,
//   expressAsyncHandler(async (req, res) => {
//     const user = await User.findById(req.user._id);
//     if (user) {
//       user.name = req.body.name || user.name;
//       user.email = req.body.email || user.email;
//       if (user.isSeller) {
//         user.seller.name = req.body.sellerName || user.seller.name;
//         user.seller.logo = req.body.sellerLogo || user.seller.logo;
//         user.seller.description =
//           req.body.sellerDescription || user.seller.description;
//       }
//       if (req.body.password) {
//         user.password = bcrypt.hashSync(req.body.password, 8);
//       }
//       const updatedUser = await user.save();
//       res.send({
//         _id: updatedUser._id,
//         name: updatedUser.name,
//         email: updatedUser.email,
//         isAdmin: updatedUser.isAdmin,
//         isSeller: user.isSeller,
//         token: generateToken(updatedUser),
//       });
//     }
//   })
// );

userRouter.get(
  '/',
  (req, res) => {
    let sql = "SELECT * FROM users";
    let sql1 = db.query(sql, (err, results) => {
      if (err) throw err;
      res.send(results);
    });
  }
);

// userRouter.delete(
//   '/:id',
//   isAuth,
//   isAdmin,
//   expressAsyncHandler(async (req, res) => {
//     const user = await User.findById(req.params.id);
//     if (user) {
//       if (user.email === 'admin@example.com') {
//         res.status(400).send({ message: 'Can Not Delete Admin User' });
//         return;
//       }
//       const deleteUser = await user.remove();
//       res.send({ message: 'User Deleted', user: deleteUser });
//     } else {
//       res.status(404).send({ message: 'User Not Found' });
//     }
//   })
// );

// userRouter.put(
//   '/:id',
//   isAuth,
//   isAdmin,
//   expressAsyncHandler(async (req, res) => {
//     const user = await User.findById(req.params.id);
//     if (user) {
//       if (user.status == false) {
//         var transporter = nodemailer.createTransport({
//           service: 'gmail',
//           auth: {
//             user: 'endridogjani4@gmail.com',
//             pass: 'prodental'
//           }
//         });
//         var mailOption = {
//           from: "endridogjani4@gmail.com",
//           to: user.email,
//           subject: 'ProDental SignUp Verification',
//           html: "<h1>Welcome To ProDental! </h1><p>\
//                     <h3>Hello "+ user.name + " </h3>\
//                     Your registration is approved!<br/>\
//                     Thank you ,<br/>\
//                     ProDental<br/>\
//                     </p>"
//         }

//         transporter.sendMail(mailOption, function (error, info) {
//           if (error) {
//             console.log(error);
//           } else {
//             console.log('Email sent: ' + info.response);
//           }
//         })
//       } else {
//         var transporter = nodemailer.createTransport({
//           service: 'gmail',
//           auth: {
//             user: 'endridogjani4@gmail.com',
//             pass: 'prodental'
//           }
//         });
//         var mailOption = {
//           from: "endridogjani4@gmail.com",
//           to: user.email,
//           subject: 'ProDental SignUp Verification',
//           html: "<h1>Welcome To ProDental! </h1><p>\
//                     <h3>Hello "+ user.name + " </h3>\
//                     We are sorry to inform you that your registration is NOT approved!<br/>\
//                     Thank you ,<br/>\
//                     ProDental<br/>\
//                     </p>"
//         }

//         transporter.sendMail(mailOption, function (error, info) {
//           if (error) {
//             console.log(error);
//           } else {
//             console.log('Email sent: ' + info.response);
//           }
//         })
//       }
//       user.name = req.body.name || user.name;
//       user.email = req.body.email || user.email;
//       user.status = Boolean(req.body.status);
//       user.isSeller = Boolean(req.body.isSeller);
//       user.isAdmin = Boolean(req.body.isAdmin);
//       // user.isAdmin = req.body.isAdmin || user.isAdmin;
//       const updatedUser = await user.save();
//       res.send({ message: 'User Updated', user: updatedUser });
//     } else {
//       res.status(404).send({ message: 'User Not Found' });
//     }
//   })
// );

// userRouter.post(
//   '/reset:id',
//   expressAsyncHandler(async (req, res) => {
//     const user = await User.findOne({ email: req.body.email, id: req.body._id });
//     if (user) {
//       var transporter = nodemailer.createTransport({
//         service: 'gmail',
//         auth: {
//           user: 'endridogjani4@gmail.com',
//           pass: 'prodental'
//         }
//       });
//       var mailOption = {
//         from: "endridogjani4@gmail.com",
//         to: user.email,
//         subject: 'Password Reset',
//         html: "<h1>Welcome To ProDental! </h1><p>\
//         <h3>Hello "+ user.name + "</h3>\
//         If You are requested to reset your password then click below link<br/><b>\
//         <a href='https://prodental.al/profile/"+ user.id + "'>Click On This Link</a><b>\
//         </p>"
//       }

//       transporter.sendMail(mailOption, function (error, info) {
//         if (error) {
//           console.log(error);
//         } else {
//           console.log('Email sent: ' + info.response);
//         }
//       })
//     } else {
//       return res.status(404).json({
//         success: false,
//         msg: "Email is not register",
//       })
//     }
//   })
// );
export default userRouter;
